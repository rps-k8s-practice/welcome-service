module welcome-service

require (
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/gin-contrib/sse v0.0.0-20190125020943-a7658810eb74 // indirect
	github.com/go-openapi/analysis v0.18.0 // indirect
	github.com/go-openapi/errors v0.18.0
	github.com/go-openapi/loads v0.18.0 // indirect
	github.com/go-openapi/runtime v0.18.0
	github.com/go-openapi/strfmt v0.18.0
	github.com/go-openapi/swag v0.18.0
	github.com/go-openapi/validate v0.18.0 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/labstack/echo/v4 v4.0.0
	github.com/mailru/easyjson v0.0.0-20190221075403-6243d8e04c3f // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/swaggo/echo-swagger/v2 v2.0.0-20190219082602-1a361fc821b8
	github.com/swaggo/gin-swagger v1.1.0 // indirect
	github.com/swaggo/swag v1.4.1
	github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43 // indirect
	golang.org/x/crypto v0.0.0-20190219172222-a4c6cb3142f2 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223 // indirect
	golang.org/x/tools v0.0.0-20190221204921-83362c3779f5 // indirect
)
