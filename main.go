package main

import (
	"welcome-service/handlers"

	_ "welcome-service/docs"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger/v2"
)

// @title RPS Welcome Service for k8s
// @version 1.0
// @description This is a sample service for k8s.
// @termsOfService http://swagger.io/terms/

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /v1
func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	e.GET("/v1/ping", handlers.Ping)

	e.Logger.Fatal(e.Start(":8080"))
}
