FROM golang:1.11.5 as build

WORKDIR /build

ADD go.mod /build/go.mod
ADD go.sum /build/go.sum

RUN go get -u

# For swagger docs update on build stage
# RUN go get github.com/swaggo/swag/cmd/swag

COPY . .

# For swagger docs update on build stage
# RUN swag init

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app

FROM alpine:latest as app

COPY --from=build /build/docs /docs

COPY --from=build /build/app /app

CMD ["/app"]
