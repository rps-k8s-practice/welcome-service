
# Image
## Build image

```bash
$ docker build -t <IMAGE> --target app .
```

IMAGE example: `registry.campany.com/repo/welcome-service:latest`

## Push image to registry

```
$ docker push <IMAGE>
```

# kubectl
## Setup kubectl

Download config file from your cluster provider and execute 

```bash
$ export KUBECONFIG=<PATH_TO_CONFIG>
```

## Deploy application

```bash
$ kubectl create -f kubernetes/deployment.yaml
$ kubectl create -f kubernetes/service.yaml
```

# Helm

## Initialize helm

```
$ helm init
```
If tiller already installed

```
$ helm init --client-only
```

If cluster with rbac -> create file rbac-config.yaml
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
```
and run command:

```
$ kubectl create -f rbac-config.yaml
```
Then initialize helm
```
$ helm init --service-account tiller
```
[More...](https://helm.sh/docs/using_helm/#role-based-access-control)
## Deploy chart
```
helm upgrade welcome-service -i --description "Install" --namespace=rps-k8s-practice ./helm
```
## Update release

```
$ docker build -t <IMAGE_NAME>:<IMAGE_TAG> --target app .
$ docker push <IMAGE_NAME>:<IMAGE_TAG>
$ helm upgrade welcome-service -i --description "Update description" --namespace=rps-k8s-practice --set ImageName=<IMAGE_NAME> ImageTag=<IMAGE_TAG> ./helm
```

# For private docker registry

Add login credentials to namespace of your application

```
$ kubectl -n rps-k8s-practice create secret docker-registry regcred --docker-server=<REGISTRY_HOST> --docker-username=<USERNAME> --docker-password=<PASSWORD> --docker-email=<EMAIL>
```
Edit deployment file: add `imagePullSecrets` in template spec

```yaml
imagePullSecrets:
- name: regcred
```
[More...](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)

# Usefull links

## Kubernetes dashboard

- [Installation](https://github.com/kubernetes/dashboard#getting-started)
- [Get access token](https://github.com/kubernetes/dashboard/wiki/Creating-sample-user)

## Ingress

Basic for most of clouds:
```
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml
```
[More information](https://kubernetes.github.io/ingress-nginx/deploy/)

## Installing cert-manager
[Docs](https://docs.cert-manager.io/en/latest/getting-started/install.html)
