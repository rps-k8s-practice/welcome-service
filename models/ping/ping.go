package models

import (
	versionService "welcome-service/clients/version_service"
	"welcome-service/clients/version_service/operations"
)

type Ping interface{}

type ping struct {
	Ping    string `json:"ping"`
	Version string `json:"version"`
}

func New() Ping {
	version := "unknown"
	resp, err := versionService.Default.Operations.GetVersion(operations.NewGetVersionParams())
	if err == nil {
		version = resp.Payload.Version
	}
	return ping{"pong", version}
}
